
### Pizza Hut Sri Lanka

## Team Members

| Student ID | Student Name            | Github Username       | Functions                                           |
|------------|-------------------------|-----------------------|-----------------------------------------------------|
| IT19014128 | A.M.W.W.R.L. Wataketiya | RavinduLa             | Pizza Menu, Cart                                    |
| IT19180526 | S.A.N.L.D. Chandrasiri  | paradocx96            | Appetizers and Others Menu, Promos Menu, Contact us |  

<!-- 
| IT19240848 | H.G. Malwatta           | DeclanChirush         | User Login & Signup, User Profile                   |
| IT19057248 | G.L.I.R. Liyanage       | ishinir               | Store Locator, Coupon Codes                         |  
 -->
